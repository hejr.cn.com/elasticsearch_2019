package com.hejr.controller;

import com.hejr.dao.UserRepository;
import com.hejr.entity.UserEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * @author: hejr
 * @desc:
 * @date: 2019/4/10 14:37
 */
@RestController
@Slf4j
public class EsController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping("/addUser")
    public UserEntity addUser(UserEntity user) {
        return userRepository.save(user);
    }

    @RequestMapping("/findUser")
    public Optional<UserEntity> findUser(String id) {
        return userRepository.findById(id);
    }

}
