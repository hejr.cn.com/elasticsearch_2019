package com.hejr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@SpringBootApplication
@EnableElasticsearchRepositories(basePackages = "com.hejr.dao")
public class Es001Application {

	public static void main(String[] args) {
		SpringApplication.run(Es001Application.class, args);
	}

}
