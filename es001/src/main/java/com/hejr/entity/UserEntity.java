package com.hejr.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * @author: hejr
 * @desc:
 * @date: 2019/4/10 14:35
 */
@Data
@Document(indexName = "hszsd", type = "user")
public class UserEntity {

    @Id
    private String id;

    private String name;

    private Integer sex;

    private Integer age;
}
