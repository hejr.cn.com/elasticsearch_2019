package com.hejr.dao;

import com.hejr.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

/**
 * @author: hejr
 * @desc:
 * @date: 2019/4/10 14:37
 */
@Component
public interface UserRepository extends CrudRepository<UserEntity, String> {
}
